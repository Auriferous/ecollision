// Generated by CoffeeScript 1.10.0
(function() {
  var ECollision, ECollisionSettings, EventManager, Graph, Interpolator, Overlay, Simulation, SimulationEngine,
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  SimulationEngine = require("./engine/simulation-engine");

  Simulation = require("./ui/simulation");

  Graph = require("./ui/graph");

  Overlay = require("./ui/overlay");

  ECollisionSettings = require("./settings");

  EventManager = require("./events/event-manager");

  Interpolator = require("./interpolator");

  module.exports = ECollision = (function() {
    var setSpeedConst, setUpdateRate;

    function ECollision(settings) {
      this.settings = settings;
      this.tick = bind(this.tick, this);
      this.engine = new SimulationEngine(this.settings.simulation.simulationWidth, this.settings.simulation.simulationHeight, this.settings);
      this.interpol = new Interpolator(this.settings.global.refreshRate, this.settings.global.updateRate);
      this.interpol.lockFPS = true;
      this.simulationUI = new Simulation(this.settings.simulation.simulationCanvas, this.engine, this.interpol, this.settings);
      this.graphUI = new Graph(this.settings.graph.graphCanvas, this.engine, 1 / 50, 5, this.settings);
      this.overlayUI = new Overlay(this.settings.overlay.overlayCanvas, this.simulationUI, this.settings);
      this.paused = false;
      this.fpsCount = this.fps = this.fpsTime = 0;
      this.updateRate = this.updateTime = this.refreshTime = 0;
      this.widgets = [this.simulationUI, this.graphUI, this.overlayUI];
      this.interpol.addListener("update", (function(_this) {
        return function() {
          if (!_this.paused) {
            return _this.update();
          }
        };
      })(this)).addListener("render", this.tick);
      this.updateRate = this.settings.global.updateRate;
      this.updateTime = 1000.0 / this.updateRate;
      this.refreshTime = 1000 / this.settings.global.refreshRate;
      EventManager.eventify(this);
    }

    ECollision.prototype.start = function() {
      var i, len, ref, widget;
      ref = this.widgets;
      for (i = 0, len = ref.length; i < len; i++) {
        widget = ref[i];
        widget.init();
      }
      return this.interpol.start();
    };

    ECollision.prototype.restart = function() {
      var i, len, ref, results, widget;
      ref = this.widgets;
      results = [];
      for (i = 0, len = ref.length; i < len; i++) {
        widget = ref[i];
        results.push(widget.restart());
      }
      return results;
    };

    ECollision.prototype.resume = function() {
      var i, len, ref, results, widget;
      this.paused = false;
      ref = this.widgets;
      results = [];
      for (i = 0, len = ref.length; i < len; i++) {
        widget = ref[i];
        results.push(widget.resume());
      }
      return results;
    };

    ECollision.prototype.pause = function() {
      var i, len, ref, results, widget;
      this.paused = true;
      ref = this.widgets;
      results = [];
      for (i = 0, len = ref.length; i < len; i++) {
        widget = ref[i];
        results.push(widget.pause());
      }
      return results;
    };

    ECollision.prototype.stop = function() {
      if (this.thread !== -1) {
        clearInterval(this.thread);
        return this.thread = -1;
      }
    };

    ECollision.prototype.getUpdateRate = function() {
      return this.updateRate;
    };

    ECollision.prototype.getUpdateTime = function() {
      return this.updateTime;
    };

    setUpdateRate = function(rate) {
      this.updateRate = rate;
      return this.updateTime = 1000.0 / this.updateRate;
    };

    setSpeedConst = function(speedConst) {
      return this.engine.speedConst = speedConst;
    };

    ECollision.prototype.update = function() {
      return this.engine.update();
    };

    ECollision.prototype.tick = function(interpolation) {
      var i, len, ref, widget;
      this.fpsCurTime = new Date().getTime();
      this.fpsCount++;
      if (this.fpsCurTime - this.fpsTime >= 1000) {
        this.fps = this.fpsCount;
        this.fpsCount = 0;
        this.fpsTime = this.fpsCurTime;
      }
      ref = this.widgets;
      for (i = 0, len = ref.length; i < len; i++) {
        widget = ref[i];
        widget.draw(interpolation);
      }
      return this.fire('tick', [interpolation]);
    };

    return ECollision;

  })();

}).call(this);
