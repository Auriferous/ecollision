Renderer = require("./renderer")

module.exports = class SimulationRenderer extends Renderer
	constructor: (@canvas, @interpolator) ->

	addParticle: (particle) ->

	removeParticle: (particle) ->